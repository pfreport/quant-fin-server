numpy==1.15.4
scipy==1.1.0
dic==1.5.2b1
matplotlib==2.2.0
openpyxl==2.5.9
pandas==0.23.4
scikit-learn==0.20.0
xlrd==1.1.0
emails==0.5.15
Jinja2==2.10
weasyprint==43
sqlalchemy==1.2.14
seaborn==0.9.0
pyodbc==4.0.24
cx_Oracle==7.0.0
statsmodels==0.8.0
arch==4.6.0
quandl==3.4.4
beautifulsoup4==4.6.3
mockito==1.1.1
xarray==0.11.0
TA-Lib==0.4.17

# Backend dependencies
Django==2.1.3
djangorestframework==3.9.0
psycopg2==2.7.6.1
django-cors-headers==2.4.0
djangorestframework-jwt==1.11.0

# NOTE: See the README.md file for other dependencies that need to be installed.
