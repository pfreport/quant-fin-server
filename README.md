# OpenShift Docker Images

This repo contains two docker images. `Dockerfile` is a Linux (CentOS 7 s2i) Docker Image, and `Dockerfile-Windows` is the windows equivilent. Note that the `Dockerfile-Windows` is out of date, but may prove useful to keep around.

## Python Versioning

Current image version which works for both the Test Server (Gitlab) and Production Server (OpenShift) is `python3.6.3_djfix`.

### Python 3.5.1

* Change the base Docker image to `centos/python-35-centos7`
* Change the line which installs `rh-python35-python-tkinter-x.x.x-xx.el7.x86_64` to `rh-python35-python-tkinter-3.5.1-11.el7.x86_64`

Build the image and you're done. Make sure you do not install two `rh-python35-python-tkinter` packages. Only install the one which matches the current python version.

### Python 3.6.3

* Change the base Docker image to `centos/python-36-centos7`
* Change the line which installs `rh-python35-python-tkinter-x.x.x-xx.el7.x86_64` to `rh-python36-python-tkinter-3.6.3-1.el7.x86_64`

Build the image and you're done. Make sure you do not install two `rh-python35-python-tkinter` packages. Only install the one which matches the current python version.

## S2I Scripts

The S2I (Source 2 Image) scripts are what controls the deployment on OpenShift. The `assemble` script is run when the image is built, so this should handle all potential dynamic dependencies (such as `pip` requirements). The `run` script is run when the image is deployed, so it should handle all neccessary runtime configuration. I.e., set up any tunnels, migrate databases and then actually start the server.

## Image Scripts

These scripts are mainly used for CRON jobs, because the `run` script is only run for deployment configurations.

### Set User

`set-user` is a script that will create a `/etc/passwd` entry for the current user. Because of the way OpenShift works, a random UID is assigned when you terminal in. However, some functions, such as `ssh` require a `/etc/passwd` entry for the running user. This script will sort that out for you.

Usage:

* Run `./set-user`

### Set Tunnel

`set-tunnel` is a script that will set up a tunnel to the pfserver.

Usage:

* Run `./set-tunnel`

### Run Configs

`run-configs` is mainly, as the name might suggest, used for generating reports or anything with a run config file. It combines the above two scripts, and will run the specified run config file (it searches for the file in the `/src/quantfin/runconfigs/` directory).

Usage:

* Run `./run-configs NAME_OF_REPORT`.
  * For example: `./run-configs sensitivity_report_run_config` will generate the sensitivies report. Notice you do not need to append the `.py` to the file name as this is done by the script.

To run a report in test mode, append a `-t` flag to the command. After the `-t` flag, you can add a single email address that the report will be sent to.
